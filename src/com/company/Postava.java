package com.company;

import java.util.ArrayList;

public class Postava {

    private int poziciaX;
    private int poziciaY;
    private int volnost;

    public int getPoziciaX() {
        return poziciaX;
    }

    public void setPoziciaX(int poziciaX) {
        this.poziciaX = poziciaX;
    }

    public int getPoziciaY() {
        return poziciaY;
    }

    public void setPoziciaY(int poziciaY) {
        this.poziciaY = poziciaY;
    }

    public int getVolnost() {
        return volnost;
    }

    public void setVolnost(int volnost) {
        this.volnost = volnost;
    }

    public void maximalnaVolnost(HernePole hernePole){
        //        Kontrolujem vsetky smery
        if (this.poziciaX+1 <= HernePole.getVelkost() && this.poziciaY+1 <= HernePole.getVelkost()){
            this.volnost++;
        }
        if (this.poziciaX-1 >= 0 && this.poziciaY+1 <= HernePole.getVelkost()){
            this.volnost++;
        }
        if (this.poziciaX+1 <= HernePole.getVelkost() && this.poziciaY-1 >= 0){
            this.volnost++;
        }
        if (this.poziciaX-1 >= 0 && this.poziciaY-1 >= 0){
            this.volnost++;
        }
        if (this.poziciaX+1 <= HernePole.getVelkost() && this.poziciaY <= HernePole.getVelkost()){
            this.volnost++;
        }
        if (this.poziciaX-1 >= 0 && this.poziciaY <= HernePole.getVelkost()){
            this.volnost++;
        }
        if (this.poziciaX <= HernePole.getVelkost() && this.poziciaY+1 <= HernePole.getVelkost()){
            this.volnost++;
        }
        if (this.poziciaX <= HernePole.getVelkost() && this.poziciaY-1 <= 0){
            this.volnost++;
        }
    }

    public void pocitajVolnost(ArrayList<Postava> postavy) {
        //        Kontrolujem vsetky smery
        for (Postava postava : postavy) {
            //
            if (postava.getPoziciaX() == this.poziciaX + 1 && postava.getPoziciaY() == this.poziciaY + 1) {
                this.volnost--;
            }
            if (postava.getPoziciaX() == this.poziciaX - 1 && postava.getPoziciaY() == this.poziciaY + 1) {
                this.volnost--;
            }
            if (postava.getPoziciaX() == this.poziciaX + 1 && postava.getPoziciaY() == this.poziciaY - 1) {
                this.volnost--;
            }
            if (postava.getPoziciaX() == this.poziciaX - 1 && postava.getPoziciaY() == this.poziciaY - 1) {
                this.volnost--;
            }
            if (postava.getPoziciaX() == this.poziciaX + 1 && postava.getPoziciaY() == this.poziciaY) {
                this.volnost--;
            }
            if (postava.getPoziciaX() == this.poziciaX - 1 && postava.getPoziciaY() == this.poziciaY) {
                this.volnost--;
            }
            if (postava.getPoziciaX() == this.poziciaX && postava.getPoziciaY() == this.poziciaY + 1) {
                this.volnost--;
            }
            if (postava.getPoziciaX() == this.poziciaX && postava.getPoziciaY() == this.poziciaY - 1) {
                this.volnost--;
            }
        }
        System.out.println("Volnost je :"+ this.volnost);
    }
}
