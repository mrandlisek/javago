package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        HernePole hernePole = new HernePole();
        boolean hra;
        do {
            hra = kolo(hernePole);
        } while (hra);
    }


    public static boolean kolo(HernePole hernePole) {
        tah(hernePole);
        for ( int i = hernePole.getBiely().size()-1; i>=0 ; i--) {
            hernePole.getBiely().get(i).pocitajVolnost(hernePole.getCierny());
            if (hernePole.getBiely().get(i).getVolnost() <= 0){
                hernePole.getBiely().remove(i);
            }
        }
        for ( int i = hernePole.getCierny().size()-1; i>=0 ; i--) {
            hernePole.getCierny().get(i).pocitajVolnost(hernePole.getBiely());
            if (hernePole.getCierny().get(i).getVolnost() <= 0){
                hernePole.getCierny().remove(i);
            }
        }
        kresli(hernePole);
        return true;
    }

    public static void tah(HernePole hernePole) {
        Scanner in = new Scanner(System.in);
//        Biely
        System.out.println("Je na tahu biely");
        System.out.println("Zadaj poziciu x");
        int bX = in.nextInt();
        System.out.println("Zadaj poziciu y");
        int bY = in.nextInt();
        Postava biely = new Postava();
        biely.setPoziciaX(bX);
        biely.setPoziciaY(bY);
        biely.maximalnaVolnost(hernePole);
        if (hernePole.getBiely()==null){
            hernePole.setBiely(new ArrayList<Postava>());
            hernePole.getBiely().add(biely);
        }else{
            hernePole.getBiely().add(biely);
        }


//      Cierny
        System.out.println("Je na tahu cierny");
        System.out.println("Zadaj poziciu x");
        int cX = in.nextInt();
        System.out.println("Zadaj poziciu y");
        int cY = in.nextInt();
        Postava cierny = new Postava();
        cierny.setPoziciaX(cX);
        cierny.setPoziciaY(cY);
        cierny.maximalnaVolnost(hernePole);
        if (hernePole.getCierny()==null){
            hernePole.setCierny(new ArrayList<Postava>());
            hernePole.getCierny().add(cierny);
        }else{
            hernePole.getCierny().add(cierny);
        }

    }

    public static void kresli(HernePole hernePole) {
        for (int i = 0; i < HernePole.getVelkost(); i++) {
            for (int j = 0; j < HernePole.getVelkost(); j++) {
                boolean printed = false;
                for (Postava postava : hernePole.getBiely()) {
                    if (postava.getPoziciaX() == i && postava.getPoziciaY() == j) {
                        System.out.print('x');
                        printed = true;
                    }
                }
                for (Postava postava : hernePole.getCierny()) {
                    if (postava.getPoziciaX() == i && postava.getPoziciaY() == j) {
                        System.out.print('O');
                        printed = true;
                    }
                }
                if (!printed) {
                    System.out.print('-');
                }
            }
            System.out.println();
        }
    }
}
