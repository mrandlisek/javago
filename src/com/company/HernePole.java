package com.company;

import java.util.ArrayList;

public class HernePole {
    private static int velkost = 19;

    private ArrayList<Postava> cierny;
    private ArrayList<Postava> biely;


    public static int getVelkost() {
        return velkost;
    }

    public ArrayList<Postava> getCierny() {
        return cierny;
    }

    public void setCierny(ArrayList<Postava> cierny) {
        this.cierny = cierny;
    }

    public ArrayList<Postava> getBiely() {
        return biely;
    }

    public void setBiely(ArrayList<Postava> biely) {
        this.biely = biely;
    }
}
